package Current Projects;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class Result{
  @SerializedName("isenableReminder")
  @Expose
  private Integer isenableReminder;
  @SerializedName("userLocation")
  @Expose
  private String userLocation;
  @SerializedName("profileImage")
  @Expose
  private String profileImage;
  @SerializedName("badgeCount")
  @Expose
  private Integer badgeCount;
  @SerializedName("userprogress")
  @Expose
  private Integer userprogress;
  @SerializedName("childId")
  @Expose
  private Integer childId;
  @SerializedName("userId")
  @Expose
  private Integer userId;
  @SerializedName("userpoint")
  @Expose
  private Integer userpoint;
  @SerializedName("phone")
  @Expose
  private Integer phone;
  @SerializedName("notiCount")
  @Expose
  private Integer notiCount;
  @SerializedName("name")
  @Expose
  private String name;
  @SerializedName("enableNotification")
  @Expose
  private Integer enableNotification;
  @SerializedName("email")
  @Expose
  private String email;
  @SerializedName("username")
  @Expose
  private Integer username;
  @SerializedName("registerDate")
  @Expose
  private String registerDate;
  public void setIsenableReminder(Integer isenableReminder){
   this.isenableReminder=isenableReminder;
  }
  public Integer getIsenableReminder(){
   return isenableReminder;
  }
  public void setUserLocation(String userLocation){
   this.userLocation=userLocation;
  }
  public String getUserLocation(){
   return userLocation;
  }
  public void setProfileImage(String profileImage){
   this.profileImage=profileImage;
  }
  public String getProfileImage(){
   return profileImage;
  }
  public void setBadgeCount(Integer badgeCount){
   this.badgeCount=badgeCount;
  }
  public Integer getBadgeCount(){
   return badgeCount;
  }
  public void setUserprogress(Integer userprogress){
   this.userprogress=userprogress;
  }
  public Integer getUserprogress(){
   return userprogress;
  }
  public void setChildId(Integer childId){
   this.childId=childId;
  }
  public Integer getChildId(){
   return childId;
  }
  public void setUserId(Integer userId){
   this.userId=userId;
  }
  public Integer getUserId(){
   return userId;
  }
  public void setUserpoint(Integer userpoint){
   this.userpoint=userpoint;
  }
  public Integer getUserpoint(){
   return userpoint;
  }
  public void setPhone(Integer phone){
   this.phone=phone;
  }
  public Integer getPhone(){
   return phone;
  }
  public void setNotiCount(Integer notiCount){
   this.notiCount=notiCount;
  }
  public Integer getNotiCount(){
   return notiCount;
  }
  public void setName(String name){
   this.name=name;
  }
  public String getName(){
   return name;
  }
  public void setEnableNotification(Integer enableNotification){
   this.enableNotification=enableNotification;
  }
  public Integer getEnableNotification(){
   return enableNotification;
  }
  public void setEmail(String email){
   this.email=email;
  }
  public String getEmail(){
   return email;
  }
  public void setUsername(Integer username){
   this.username=username;
  }
  public Integer getUsername(){
   return username;
  }
  public void setRegisterDate(String registerDate){
   this.registerDate=registerDate;
  }
  public String getRegisterDate(){
   return registerDate;
  }
}