package com.example.admin.apicalldemo.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.admin.apicalldemo.Method.Itemlist;
import com.example.admin.apicalldemo.Method.ItemlistResult;
import com.example.admin.apicalldemo.R;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewholder>{

    public List<ItemlistResult> itemlists;
    public Activity activity;

    public RecyclerViewAdapter(List <ItemlistResult> itemlists,Activity activity){
        this.activity=activity;
        this.itemlists=itemlists;


    }

    @NonNull
    @Override
    public MyViewholder onCreateViewHolder( ViewGroup parent, int viewType) {
        View ietmview= LayoutInflater.from( parent.getContext() ).inflate( R.layout.item_list,parent,false);
        return new MyViewholder( ietmview );
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewholder holder, int position) {


        //holder.image.setImageResource( Integer.parseInt( itemlists.get( position ).getImage() ) );
        holder.title.setText(itemlists.get( position ).getTitle());
        holder.postby.setText(itemlists.get( position ).getPost_by() );
        Glide.with( activity )
                .load( itemlists.get( position ).getImage() )
                .error( R.drawable.ic_launcher_background )
                .centerCrop()
                .into( holder.image );


    }

    @Override
    public int getItemCount() {
        return itemlists.size();
    }

    public class MyViewholder extends RecyclerView.ViewHolder{

        public TextView title,postby;
        public ImageView image;
        public MyViewholder(View itemView) {
            super( itemView );
            image=itemView.findViewById( R.id.civ_itemlist );
            title  =itemView.findViewById( R.id.tv_title_itemlist );
            postby=itemView.findViewById( R.id.tv_postby_itemlist );
        }
    }
}
