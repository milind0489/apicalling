package com.example.admin.apicalldemo.Webservices;



import com.example.admin.apicalldemo.Method.Itemlist;
import com.example.admin.apicalldemo.Method.ItemlistResult;
import com.example.admin.apicalldemo.Method.Login;
import com.example.admin.apicalldemo.Method.UserResult;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface WebServices {

    @Headers( ( "Authtoken:j67@45gy%$^ghu^&" ) )
    @POST(APIUrl.primary+APIUrl.Login)
    Call <Login> getphoneID(@Body JsonObject jsonObject);


    @Headers( ( "Authtoken:j67@45gy%$^ghu^&" ) )
    @POST (APIUrl.primary+APIUrl.userDetails)
    Call<UserResult> getuserid(@Body JsonObject jsonObject);

    @Headers( ( "Authtoken:j67@45gy%$^ghu^&" ) )
    @POST (APIUrl.primary+APIUrl.Itemlist)
    Call <Itemlist> getItem(@Body JsonObject jsonObject);


    /*@POST(APIUrl.primary+APIUrl.Login)
    Call <Login> getpasswordID(*//*@Body JsonObject jsonObject*//*);*/

    /*@POST(APIUrl.primary+APIUrl.Login)
    Call<Login> getPhone();*/

   /* @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.login)
    Call<LoginMaster> loginApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.userDetails)
    Call<LoginMaster> userDetailApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.sendCode)
    Call<SendCodeMaster> sendCodeApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.verifyCode)
    Call<Message> verifyOtpApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.forgotPassword)
    Call<Message> forgotPasswordApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.register)
    Call<LoginMaster> registerApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.checkuser)
    Call<LoginMaster> checkUserApi(@Body JsonObject jsonBody);


    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.k2categories)
    Call<CategoryMaster> k2categoriesApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.k2Itemlist)
    Call<ArticleMaster> articleListApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.kunnenaCategoy)
    Call<KunnenaCategoryMaster> kunnenaCategoyApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.kunnenaTopic)
    Call<KunnenaTopicMaster> kunnenaTopicApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.k2itemDetail)
    Call<ArticleDetailMaster> articleDetailsApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.k2CommentListing)
    Call<ArticleCommentMaster> articleDetailCommentApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.k2Comment)
    Call<Message> articleAddCommentApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.searchItem)
    Call<SearchItemMaster> searchItemApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.orodhaitemSearch)
    Call<OrodhaSearchMaster> orodhaSearchItemApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.favouriteClubs)
    Call<SearchItemMaster> favoriteItemApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.contactUs)
    Call<Message> contactUsApi(@Body JsonObject jsonBody);

    @Multipart
    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.createTopic)
    Call<Message> createTopicApi(@Part("catId") RequestBody categoryId,
                                 @Part("subject") RequestBody subject,
                                 @Part("message") RequestBody message,
                                 @Part("userId") RequestBody userId,
                                 @Part("catPoints") RequestBody categoryPoints,
                                 @Part("userPoint") RequestBody userPoints,
                                 @Part MultipartBody.Part[] attachment,
                                 @Part("topicType") RequestBody topicType);

    @Multipart
    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.totochanjoAdd)
    Call<Message> createChanjoAddApi(@Part("lastDate") RequestBody dob,
                                     @Part("nextDate") RequestBody nextDob,
                                     @Part("userId") RequestBody userId,
                                     @Part("childId") RequestBody childId,
                                     @Part("childWeight") RequestBody weight,
                                     @Part("childHeight") RequestBody height,
                                     @Part("chanjoId") RequestBody changoId,
                                     @Part("klinikid") RequestBody klinikiId,
                                     @Part("hospitalId") RequestBody hospitalId,
                                     @Part MultipartBody.Part[] attachment
    );


    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.clubDetail)
    Call<ClubDetailMaster> clubDetailApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.listNotifications)
    Call<NotificationMaster> notificationApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.readNotification)
    Call<Message> readNotificationApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.questionOptions)
    Call<SuggestionMaster> questionOptionsApi(@Body JsonObject jsonBody);

    @Multipart
    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.replyTopic)
    Call<Message> replyTopicApi(@Part("topicId") RequestBody topicId,
                                @Part("catId") RequestBody categoryId,
                                @Part("userId") RequestBody userId,
                                @Part("parentMsgId") RequestBody parentMsgId,
                                @Part("subject") RequestBody subject,
                                @Part("message") RequestBody message,
                                @Part MultipartBody.Part attachment,
                                @Part("enableNotify") RequestBody notification);

    @Multipart
    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.changeAvatar)
    Call<LoginMaster> updateProfileImageApi(@Part("userId") RequestBody userId,
                                            @Part MultipartBody.Part attachment);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.topicFavourite)
    Call<Message> topicFavoriteApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.questionStore)
    Call<LoginMaster> questionStoreApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.questionAnswer)
    Call<SuggestionAnswerMaster> questionAnswerApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.ushauriTopics)
    Call<QuestionAnswerMaster> ushauriTopicsApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.joinClub)
    Call<Message> joinClubApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.childFatheradd)
    Call<FatherAdd> childFatherAddApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.removeFather)
    Call<Message> removeFatherAddApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.removeChildAccount)
    Call<Message> removeChildApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.klinikiDetail)
    Call<KlinikiMaster> klinikiDetailApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.childList)
    Call<ChildListMaster> childListApi(@Body JsonObject jsonBody);

   @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.attendanceList)
    Call<AttendanceMaster> attendanceListApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.childchanjoList)
    Call<ChildChangoListMaster> childChangoListApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.reportQuestion)
    Call<Message> reportQuestionAdminApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.solvedQuestion)
    Call<Message> solvedQuestionAdminApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.audioList)
    Call<AfyaCastMaster> afyaCastApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.topicLike)
    Call<Message> topicLikeApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.orodhaCategory)
    Call<OrodhaCategoryMaster> orodhaCategoryApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.childDetail)
    Call<ChildDetailMaster> hospitalListApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.childGraphDetail)
    Call<ChildGraphMaster> childGraphDetailApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.ushauriPoints)
    Call<UshauriPoints> ushauriPointsApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.appointCategory)
    Call<OrodhaCategoryMaster> appointmentCategoryApi();

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.orodhaItems)
    Call<OrodhaItemMaster> orodhaItemApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.appointDoctorDetail)
    Call<DoctorDetailMaster> doctorSpecialistDetailApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.appointBooking)
    Call<AppointmentBookingDetailMaster> appointmentBookingDetailApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.packageList)
    Call<BuyPointsMaster> buyPointsApi();

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.doctorFavourite)
    Call<Message> doctorFavoriteApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.appBannerlist)
    Call<BannerImage> bannerImageApi();

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.reminderCatdozi)
    Call<ReminderCatDozi> reminderCategoryApi();

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.orodhaItemDetail)
    Call<OrodhaDetailMaster> orodhaItemDetailApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.downloadItems)
    Call<DArticleMaster> downloadItemApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.reminder)
    Call<Message> reminderApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.reminderListing)
    Call<ReminderMaster> reminderListingApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.reminderRemove)
    Call<Message> reminderRemoveApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.createCycle)
    Call<CycleListMaster> createCycleApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.removePartner)
    Call<Message> removePartnerApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.addPartner)
    Call<AddPartner> addPartnerApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.addnotifyPartner)
    Call<Message> addNotifyPartnerApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.cycleListing)
    Call<CycleListMaster> cycleListingApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.notismsEnable)
    Call<Message> enableDisableNotificationApi(@Body JsonObject jsonBody);

    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.settingEnable)
    Call<Message> settingApi(@Body JsonObject jsonBody);

    @Multipart
    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.totochildAdd)
    Call<Message> addChildApi(@Part("userId") RequestBody userId,
                              @Part("childName") RequestBody childName,
                              @Part("childDOB") RequestBody childDOB,
                              @Part("childWeight") RequestBody childWeight,
                              @Part("childGender") RequestBody childGender,
                              @Part("hospitalId") RequestBody hospitalId,
                              @Part("childId") RequestBody childId,
                              @Part MultipartBody.Part childImage);


    @Headers("Authtoken:"+API_Params.authToken)
    @POST(APIUrl.paymentAPI)
    Call<Message> makePaymentApi(@Body JsonObject jsonBody);*/


}
