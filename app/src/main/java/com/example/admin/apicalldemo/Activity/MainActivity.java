package com.example.admin.apicalldemo.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.apicalldemo.Method.Login;
import com.example.admin.apicalldemo.R;
import com.example.admin.apicalldemo.Webservices.ApiHandler;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.iv_login)
    ImageView ivLogin;
    @BindView(R.id.evusername_login)
    EditText evusernameLogin;
    @BindView(R.id.evpassword_login)
    EditText evpasswordLogin;
    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.tvphone)
    TextView tvphone;
    @BindView(R.id.tvpw)
    TextView tvpw;
    @BindView(R.id.tvditeal)
    TextView tvditeal;
    @BindView(R.id.btn_diteal)
    Button btnDiteal;
    @BindView(R.id.tv_listview)
    Button tvListview;


    /*String url = "https://dli.tanzmed.co.tz/";*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );
        ButterKnife.bind( this );


    }

    private Call <Login> LoginAPI() {


        final Call <Login> loginCall = ApiHandler.getApiService( ).getphoneID( userObject( ) );
        loginCall.enqueue( new Callback <Login>( ) {
            @Override
            public void onResponse(Call <Login> call, Response <Login> response) {
                if (response.isSuccessful( )) {
                    Login login = response.body( );
                    Log.e( "new", new Gson( ).toJson( login ).toString( ) );
                    int success = login.getSuccess( );
                    if (success == 1) {
                        tvphone.setText( response.body( ).toString() );
                        tvpw.setText( response.body( ).toString( ) );
                        tvditeal.setText( new Gson( ).toJson( login ).toString( ) );

                        Toast.makeText( getApplicationContext( ), " success", Toast.LENGTH_LONG ).show( );

                    } else if (success == 0) {
                        Toast.makeText( getApplicationContext( ), "not success", Toast.LENGTH_LONG ).show( );
                    }
                }
            }

            @Override
            public void onFailure(Call <Login> call, Throwable t) {
                Toast.makeText( getApplicationContext( ), "Failure success", Toast.LENGTH_LONG ).show( );

            }
        } );

        return null;
    }


    private JsonObject userObject() {
        JsonObject gsonObject = new JsonObject( );
        try {
            JSONObject jsonObj_login = new JSONObject( );
            jsonObj_login.put( "device", "2" );
            jsonObj_login.put( "deviceToken", "token" );
            jsonObj_login.put( "phone", evusernameLogin.getText( ).toString( ) );
            /*Log.e( "tag",evusernameLogin );*/
            jsonObj_login.put( "password", evpasswordLogin.getText( ).toString( ) );
            jsonObj_login.put( "isSocial", "0" );


            JsonParser jsonParser = new JsonParser( );
            gsonObject = ( JsonObject ) jsonParser.parse( jsonObj_login.toString( ) );
            Log.e( "MY gson.JSON:  ", "AS PARAMETER  " + gsonObject );


        } catch (JSONException e) {
            e.printStackTrace( );
        }

        return gsonObject;
    }


    @OnClick(R.id.btn_login)
    public void onViewClicked() {

        LoginAPI( );
    }

    @OnClick(R.id.btn_diteal)
    public void onClicked() {

        Intent intent = new Intent( MainActivity.this, UserdetailActivity.class );
        startActivity( intent );
    }

    @OnClick(R.id.tv_listview)
    public void OnClicked() {
        Intent intent = new Intent( MainActivity.this, ItemListActivity.class );
        startActivity( intent );
    }
}


