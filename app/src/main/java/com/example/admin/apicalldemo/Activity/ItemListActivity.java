package com.example.admin.apicalldemo.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.apicalldemo.Method.Itemlist;
import com.example.admin.apicalldemo.Method.ItemlistResult;
import com.example.admin.apicalldemo.R;
import com.example.admin.apicalldemo.Adapter.RecyclerViewAdapter;
import com.example.admin.apicalldemo.Webservices.ApiHandler;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ItemListActivity extends AppCompatActivity {

    @BindView(R.id.rv_Itemlist)
    RecyclerView rvItemlist;

    public RecyclerViewAdapter recyclerViewAdapter;
    public List <ItemlistResult> itemlistResults= new ArrayList <>( );


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.item_list_activity );
        ButterKnife.bind( this );

        recyclerViewAdapter=new RecyclerViewAdapter(itemlistResults, this );
        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager( getApplicationContext() );
        rvItemlist.setLayoutManager( layoutManager );
        rvItemlist.setItemAnimator( new DefaultItemAnimator() );
        rvItemlist.setAdapter( recyclerViewAdapter );
        Call <Itemlist> itemlistCall= ApiHandler.getApiService().getItem( gsonobject() );

        itemlistCall.enqueue( new Callback <Itemlist>( ) {
            @Override
            public void onResponse(Call <Itemlist> call, Response<Itemlist> response) {


                Itemlist itemlist = response.body( );
                Log.e( "FINAL STRING: ", new Gson( ).toJson( itemlist ).toString( ) );

                int success = itemlist.getSuccess( );
                if (success == 1) {

                    if(itemlist.getResult()!=null) {
                        itemlistResults.clear();
                        itemlistResults.addAll( itemlist.getResult( ) );
                        recyclerViewAdapter.notifyDataSetChanged( );
                    }

                    Toast.makeText( getApplicationContext( ), " success", Toast.LENGTH_LONG ).show( );
                } else if (success == 0) {
                    Toast.makeText( getApplicationContext( ), "not success", Toast.LENGTH_LONG ).show( );

                }
            }


            @Override
            public void onFailure(Call <Itemlist> call, Throwable t) {
                Toast.makeText( getApplicationContext( ), "not success", Toast.LENGTH_LONG ).show( );
            }
        } );
        }

    private JsonObject gsonobject() {

        JsonObject gsonObjectt= new JsonObject(  );
        try {

            JSONObject jsonObject=new JSONObject(  );
            jsonObject.put( "catId","24" );
            jsonObject.put( "startLimit",0 );
            jsonObject.put( "isDownload","0" );
            Log.e("tag","error"+jsonObject);

            JsonParser jsonParser=new JsonParser();
            gsonObjectt=(JsonObject )  jsonParser.parse( jsonObject.toString() );
            Log.e( "MY gson.JSON:  ", "AS PARAMETER  " + gsonObjectt );

        } catch (Exception e) {
            e.printStackTrace( );
        }

        Log.e("tag","error2"+gsonObjectt);
        return gsonObjectt;
    }
}
