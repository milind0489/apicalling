package com.example.admin.apicalldemo.Method;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class Userdetail {

    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("userpoint")
    @Expose
    private String userpoint;
    @SerializedName("profileImage")
    @Expose
    private String profileImage;
    @SerializedName("badgeCount")
    @Expose
    private Integer badgeCount;
    @SerializedName("registerDate")
    @Expose
    private String registerDate;
    @SerializedName("userprogress")
    @Expose
    private Integer userprogress;
    @SerializedName("notiCount")
    @Expose
    private String notiCount;
    @SerializedName("enableNotification")
    @Expose
    private String enableNotification;
    @SerializedName("isenableReminder")
    @Expose
    private String isenableReminder;
    @SerializedName("userLocation")
    @Expose
    private String userLocation;
    @SerializedName("childId")
    @Expose
    private Integer childId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserpoint() {
        return userpoint;
    }

    public void setUserpoint(String userpoint) {
        this.userpoint = userpoint;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public Integer getBadgeCount() {
        return badgeCount;
    }

    public void setBadgeCount(Integer badgeCount) {
        this.badgeCount = badgeCount;
    }

    public String getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(String registerDate) {
        this.registerDate = registerDate;
    }

    public Integer getUserprogress() {
        return userprogress;
    }

    public void setUserprogress(Integer userprogress) {
        this.userprogress = userprogress;
    }

    public String getNotiCount() {
        return notiCount;
    }

    public void setNotiCount(String notiCount) {
        this.notiCount = notiCount;
    }

    public String getEnableNotification() {
        return enableNotification;
    }

    public void setEnableNotification(String enableNotification) {
        this.enableNotification = enableNotification;
    }

    public String getIsenableReminder() {
        return isenableReminder;
    }

    public void setIsenableReminder(String isenableReminder) {
        this.isenableReminder = isenableReminder;
    }

    public String getUserLocation() {
        return userLocation;
    }

    public void setUserLocation(String userLocation) {
        this.userLocation = userLocation;
    }

    public Integer getChildId() {
        return childId;
    }

    public void setChildId(Integer childId) {
        this.childId = childId;
    }


}
