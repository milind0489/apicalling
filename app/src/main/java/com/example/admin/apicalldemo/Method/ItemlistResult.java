package com.example.admin.apicalldemo.Method;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class ItemlistResult {
    /*@SerializedName( "itemlistResult" )
    private ArrayList<temlistResult> itemlistResults;

    public ArrayList <itemlistResult> getItemlistResults() {
        return itemlistResults;
    }

    public void setItemlistResults(ArrayList <itemlistResult> itemlistResults) {
        this.itemlistResults = itemlistResults;
    }
*/
    @SerializedName("post_on")
    @Expose
    private String post_on;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("width")
    @Expose
    private Integer width;
    @SerializedName("post_by")
    @Expose
    private String post_by;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("height")
    @Expose
    private Integer height;

    public ItemlistResult(String title) {
        this.title=title;
    }

    public void setPost_on(String post_on){
        this.post_on=post_on;
    }
    public String getPost_on(){
        return post_on;
    }
    public void setImage(String image){
        this.image=image;
    }
    public String getImage(){
        return image;
    }
    public void setWidth(Integer width){
        this.width=width;
    }
    public Integer getWidth(){
        return width;
    }
    public void setPost_by(String post_by){
        this.post_by=post_by;
    }
    public String getPost_by(){
        return post_by;
    }
    public void setId(Integer id){
        this.id=id;
    }
    public Integer getId(){
        return id;
    }
    public void setCategory(String category){
        this.category=category;
    }
    public String getCategory(){
        return category;
    }
    public void setTitle(String title){
        this.title=title;
    }
    public String getTitle(){
        return title;
    }
    public void setHeight(Integer height){
        this.height=height;
    }
    public Integer getHeight(){
        return height;
    }
}
/**
 * Awesome Pojo Generator
        * */
