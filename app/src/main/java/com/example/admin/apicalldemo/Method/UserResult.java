package com.example.admin.apicalldemo.Method;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class UserResult {

    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("result")
    @Expose
    private LoginResult result;

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public LoginResult getResult() {
        return result;
    }

    public void setResult(LoginResult result) {
        this.result = result;
    }
}

