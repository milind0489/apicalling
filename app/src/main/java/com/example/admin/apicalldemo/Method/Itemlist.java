package com.example.admin.apicalldemo.Method;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;
/**
 * Awesome Pojo Generator
 * */
public class Itemlist{
  @SerializedName("result")
  @Expose
  private List<ItemlistResult> result;
  @SerializedName("success")
  @Expose
  private Integer success;

  public void setResult(List<ItemlistResult> result){
   this.result=result;
  }
  public List<ItemlistResult> getResult(){
   return result;
  }
  public void setSuccess(Integer success){
   this.success=success;
  }
  public Integer getSuccess(){
   return success;
  }

}