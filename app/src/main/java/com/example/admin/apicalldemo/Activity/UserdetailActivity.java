package com.example.admin.apicalldemo.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.admin.apicalldemo.Method.UserResult;
import com.example.admin.apicalldemo.R;
import com.example.admin.apicalldemo.Webservices.ApiHandler;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserdetailActivity extends AppCompatActivity {

    @BindView(R.id.et_userditeal)
    TextView etUserditeal;
    @BindView(R.id.btn_ok_userdetail)
    Button btnOkUserdetail;
    @BindView(R.id.tv_userditeal_userId)
    TextView tvUserditealUserId;
    @BindView(R.id.tv_userditeal_name)
    TextView tvUserditealName;
    @BindView(R.id.tv_userditeal_email)
    TextView tvUserditealEmail;
    @BindView(R.id.tv_userditeal_phone)
    TextView tvUserditealPhone;
    @BindView(R.id.tv_userditeal_username)
    TextView tvUserditealUsername;
    @BindView(R.id.tv_userditeal_userpoint)
    TextView tvUserditealUserpoint;
    @BindView(R.id.tv_userditeal_badgeCount)
    TextView tvUserditealBadgeCount;
    @BindView(R.id.tv_userditeal_registerDate)
    TextView tvUserditealRegisterDate;
    @BindView(R.id.tv_userditeal_userprogress)
    TextView tvUserditealUserprogress;
    @BindView(R.id.tv_userditeal_notiCount)
    TextView tvUserditealNotiCount;
    @BindView(R.id.tv_userditeal_enableNotification)
    TextView tvUserditealEnableNotification;
    @BindView(R.id.tv_userditeal_isenableReminder)
    TextView tvUserditealIsenableReminder;
    @BindView(R.id.tv_userditeal_userLocation)
    TextView tvUserditealUserLocation;
    @BindView(R.id.tv_userditeal_childId)
    TextView tvUserditealChildId;
    @BindView(R.id.tv_userditeal_profileImage)
    ImageView tvUserditealProfileImage;
    @BindView(R.id.ll)
    LinearLayout ll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.userdetail_activity );
        ButterKnife.bind( this );
    }


    @OnClick(R.id.btn_ok_userdetail)
    public void onViewClicked() {
        userdetail( );
    }

    private void userdetail() {

        Call <UserResult> userResultCall = ApiHandler.getApiService( ).getuserid( gsonob( ) );


        userResultCall.enqueue( new Callback <UserResult>( ) {
            @Override
            public void onResponse(Call <UserResult> call, Response <UserResult> response) {

                if (response.isSuccessful( )) {

                    UserResult userResult = response.body( );
                    Log.e( "cn", response.toString( ) );
                    Log.e( "new", new Gson( ).toJson( userResult ).toString( ) );
                    int success = userResult.getSuccess( );
                    if (success == 1) {
                        tvUserditealUserId.setText( response.body( ).getResult( ).getUserId( ).toString( ) );
                        tvUserditealName.setText( response.body( ).getResult( ).getName( ).toString( ) );
                        tvUserditealEmail.setText( response.body( ).getResult( ).getEmail( ).toString( ) );
                        tvUserditealPhone.setText( response.body( ).getResult( ).getPhone( ).toString( ) );
                        tvUserditealUsername.setText( response.body( ).getResult( ).getUsername( ).toString( ) );
                        tvUserditealUserpoint.setText( response.body( ).getResult( ).getUserpoint( ).toString( ) );
                        Glide.with( getApplicationContext( ) )
                                .load( response.body( ).getResult( ).getProfileImage( ).toString( ) )
                                .into( tvUserditealProfileImage );
                        tvUserditealBadgeCount.setText( response.body( ).getResult( ).getBadgeCount( ).toString( ) );
                        tvUserditealRegisterDate.setText( response.body( ).getResult( ).getRegisterDate( ).toString( ) );
                        tvUserditealUserprogress.setText( response.body( ).getResult( ).getUserprogress( ).toString( ) );
                        tvUserditealNotiCount.setText( response.body( ).getResult( ).getNotiCount( ).toString( ) );
                        tvUserditealEnableNotification.setText( response.body( ).getResult( ).getEnableNotification( ).toString( ) );
                        tvUserditealIsenableReminder.setText( response.body( ).getResult( ).getIsenableReminder( ).toString( ) );
                        tvUserditealUserLocation.setText( response.body( ).getResult( ).getUserLocation( ).toString( ) );
                        tvUserditealChildId.setText( response.body( ).getResult( ).getChildId( ).toString( ) );


                        Log.e( "tag1", response.body( ).toString( ) );

                        Toast.makeText( getApplicationContext( ), " success", Toast.LENGTH_LONG ).show( );

                    } else if (success == 0) {

                        Log.e( "tag0", response.body( ).toString( ) );

                        Toast.makeText( getApplicationContext( ), "not success", Toast.LENGTH_LONG ).show( );
                    }
                }
            }

            @Override
            public void onFailure(Call <UserResult> call, Throwable t) {
                Toast.makeText( getApplicationContext( ), "Failure success", Toast.LENGTH_LONG ).show( );
                Log.e( "log", "Error" + t.getMessage( ) );
            }
        } );


    }

    private JsonObject gsonob() {
        JsonObject gsonObject = new JsonObject( );
        try {
            JSONObject jsonObject = new JSONObject( );
            jsonObject.put( "device", "2" );
            jsonObject.put( "deviceToken", "fle0oVfYjRg:APA91bHtiqLZOotpjQc-GaU6tXT4J9wAbVpdbvHL_03xSu8chKgPfnFWmRmPrYCm4bj2IlYPfDhHtp4OP1ilR9ZQerLuEkVksFSUEiH23NZGsRD90BjODh2rZ-5COvR4pGQjCl1CL3Ik" );
            jsonObject.put( "userId", etUserditeal.getText( ).toString( ) );

            JsonParser jsonParser = new JsonParser( );
            gsonObject = ( JsonObject ) jsonParser.parse( jsonObject.toString( ) );
            Log.e( "MY gson.JSON:  ", "AS PARAMETER  " + gsonObject );

        } catch (Exception e) {
            e.printStackTrace( );
        }
        Log.e( "gson", gsonObject.toString( ) );
        return gsonObject;
    }
}
