// Generated code from Butter Knife. Do not modify!
package com.example.admin.apicalldemo.Activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.example.admin.apicalldemo.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ItemListActivity_ViewBinding implements Unbinder {
  private ItemListActivity target;

  @UiThread
  public ItemListActivity_ViewBinding(ItemListActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ItemListActivity_ViewBinding(ItemListActivity target, View source) {
    this.target = target;

    target.rvItemlist = Utils.findRequiredViewAsType(source, R.id.rv_Itemlist, "field 'rvItemlist'", RecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ItemListActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.rvItemlist = null;
  }
}
