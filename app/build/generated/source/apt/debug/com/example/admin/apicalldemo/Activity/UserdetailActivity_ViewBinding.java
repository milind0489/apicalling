// Generated code from Butter Knife. Do not modify!
package com.example.admin.apicalldemo.Activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.example.admin.apicalldemo.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class UserdetailActivity_ViewBinding implements Unbinder {
  private UserdetailActivity target;

  private View view2131230758;

  @UiThread
  public UserdetailActivity_ViewBinding(UserdetailActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public UserdetailActivity_ViewBinding(final UserdetailActivity target, View source) {
    this.target = target;

    View view;
    target.etUserditeal = Utils.findRequiredViewAsType(source, R.id.et_userditeal, "field 'etUserditeal'", TextView.class);
    view = Utils.findRequiredView(source, R.id.btn_ok_userdetail, "field 'btnOkUserdetail' and method 'onViewClicked'");
    target.btnOkUserdetail = Utils.castView(view, R.id.btn_ok_userdetail, "field 'btnOkUserdetail'", Button.class);
    view2131230758 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
    target.tvUserditealUserId = Utils.findRequiredViewAsType(source, R.id.tv_userditeal_userId, "field 'tvUserditealUserId'", TextView.class);
    target.tvUserditealName = Utils.findRequiredViewAsType(source, R.id.tv_userditeal_name, "field 'tvUserditealName'", TextView.class);
    target.tvUserditealEmail = Utils.findRequiredViewAsType(source, R.id.tv_userditeal_email, "field 'tvUserditealEmail'", TextView.class);
    target.tvUserditealPhone = Utils.findRequiredViewAsType(source, R.id.tv_userditeal_phone, "field 'tvUserditealPhone'", TextView.class);
    target.tvUserditealUsername = Utils.findRequiredViewAsType(source, R.id.tv_userditeal_username, "field 'tvUserditealUsername'", TextView.class);
    target.tvUserditealUserpoint = Utils.findRequiredViewAsType(source, R.id.tv_userditeal_userpoint, "field 'tvUserditealUserpoint'", TextView.class);
    target.tvUserditealBadgeCount = Utils.findRequiredViewAsType(source, R.id.tv_userditeal_badgeCount, "field 'tvUserditealBadgeCount'", TextView.class);
    target.tvUserditealRegisterDate = Utils.findRequiredViewAsType(source, R.id.tv_userditeal_registerDate, "field 'tvUserditealRegisterDate'", TextView.class);
    target.tvUserditealUserprogress = Utils.findRequiredViewAsType(source, R.id.tv_userditeal_userprogress, "field 'tvUserditealUserprogress'", TextView.class);
    target.tvUserditealNotiCount = Utils.findRequiredViewAsType(source, R.id.tv_userditeal_notiCount, "field 'tvUserditealNotiCount'", TextView.class);
    target.tvUserditealEnableNotification = Utils.findRequiredViewAsType(source, R.id.tv_userditeal_enableNotification, "field 'tvUserditealEnableNotification'", TextView.class);
    target.tvUserditealIsenableReminder = Utils.findRequiredViewAsType(source, R.id.tv_userditeal_isenableReminder, "field 'tvUserditealIsenableReminder'", TextView.class);
    target.tvUserditealUserLocation = Utils.findRequiredViewAsType(source, R.id.tv_userditeal_userLocation, "field 'tvUserditealUserLocation'", TextView.class);
    target.tvUserditealChildId = Utils.findRequiredViewAsType(source, R.id.tv_userditeal_childId, "field 'tvUserditealChildId'", TextView.class);
    target.tvUserditealProfileImage = Utils.findRequiredViewAsType(source, R.id.tv_userditeal_profileImage, "field 'tvUserditealProfileImage'", ImageView.class);
    target.ll = Utils.findRequiredViewAsType(source, R.id.ll, "field 'll'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    UserdetailActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.etUserditeal = null;
    target.btnOkUserdetail = null;
    target.tvUserditealUserId = null;
    target.tvUserditealName = null;
    target.tvUserditealEmail = null;
    target.tvUserditealPhone = null;
    target.tvUserditealUsername = null;
    target.tvUserditealUserpoint = null;
    target.tvUserditealBadgeCount = null;
    target.tvUserditealRegisterDate = null;
    target.tvUserditealUserprogress = null;
    target.tvUserditealNotiCount = null;
    target.tvUserditealEnableNotification = null;
    target.tvUserditealIsenableReminder = null;
    target.tvUserditealUserLocation = null;
    target.tvUserditealChildId = null;
    target.tvUserditealProfileImage = null;
    target.ll = null;

    view2131230758.setOnClickListener(null);
    view2131230758 = null;
  }
}
