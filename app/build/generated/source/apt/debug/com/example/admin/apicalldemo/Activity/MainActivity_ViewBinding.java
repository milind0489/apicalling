// Generated code from Butter Knife. Do not modify!
package com.example.admin.apicalldemo.Activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.example.admin.apicalldemo.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MainActivity_ViewBinding implements Unbinder {
  private MainActivity target;

  private View view2131230757;

  private View view2131230756;

  private View view2131230916;

  @UiThread
  public MainActivity_ViewBinding(MainActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public MainActivity_ViewBinding(final MainActivity target, View source) {
    this.target = target;

    View view;
    target.ivLogin = Utils.findRequiredViewAsType(source, R.id.iv_login, "field 'ivLogin'", ImageView.class);
    target.evusernameLogin = Utils.findRequiredViewAsType(source, R.id.evusername_login, "field 'evusernameLogin'", EditText.class);
    target.evpasswordLogin = Utils.findRequiredViewAsType(source, R.id.evpassword_login, "field 'evpasswordLogin'", EditText.class);
    view = Utils.findRequiredView(source, R.id.btn_login, "field 'btnLogin' and method 'onViewClicked'");
    target.btnLogin = Utils.castView(view, R.id.btn_login, "field 'btnLogin'", Button.class);
    view2131230757 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
    target.tvphone = Utils.findRequiredViewAsType(source, R.id.tvphone, "field 'tvphone'", TextView.class);
    target.tvpw = Utils.findRequiredViewAsType(source, R.id.tvpw, "field 'tvpw'", TextView.class);
    target.tvditeal = Utils.findRequiredViewAsType(source, R.id.tvditeal, "field 'tvditeal'", TextView.class);
    view = Utils.findRequiredView(source, R.id.btn_diteal, "field 'btnDiteal' and method 'onClicked'");
    target.btnDiteal = Utils.castView(view, R.id.btn_diteal, "field 'btnDiteal'", Button.class);
    view2131230756 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClicked();
      }
    });
    view = Utils.findRequiredView(source, R.id.tv_listview, "field 'tvListview' and method 'OnClicked'");
    target.tvListview = Utils.castView(view, R.id.tv_listview, "field 'tvListview'", Button.class);
    view2131230916 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.OnClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    MainActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ivLogin = null;
    target.evusernameLogin = null;
    target.evpasswordLogin = null;
    target.btnLogin = null;
    target.tvphone = null;
    target.tvpw = null;
    target.tvditeal = null;
    target.btnDiteal = null;
    target.tvListview = null;

    view2131230757.setOnClickListener(null);
    view2131230757 = null;
    view2131230756.setOnClickListener(null);
    view2131230756 = null;
    view2131230916.setOnClickListener(null);
    view2131230916 = null;
  }
}
