package Current Projects;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class Login{
  @SerializedName("result")
  @Expose
  private Result result;
  @SerializedName("success")
  @Expose
  private Integer success;
  @SerializedName("message")
  @Expose
  private String message;
  public void setResult(Result result){
   this.result=result;
  }
  public Result getResult(){
   return result;
  }
  public void setSuccess(Integer success){
   this.success=success;
  }
  public Integer getSuccess(){
   return success;
  }
  public void setMessage(String message){
   this.message=message;
  }
  public String getMessage(){
   return message;
  }
}